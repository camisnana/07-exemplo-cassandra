package br.com.itau.pinterest.api;

public class TokenSaida {
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
